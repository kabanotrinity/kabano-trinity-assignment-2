#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

struct Node* createNode(int number) {
    struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
    if (newNode == NULL) {
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
        }

    newNode->number = number;
    newNode->next = NULL ;
    return newNode;
}
void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newNode;
}

void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

void insertAfterKey(struct Node **head, int key, int value) {
    struct Node *current = *head;
    while (current != NULL && current->number != key) {
        current = current->next;
    }
    if (current == NULL) return; // Key not found
    struct Node *newNode = createNode(value);
    newNode->next = current->next;
    current->next = newNode;
}
void deleteByKey(struct Node **head, int key) {
    if (*head == NULL) return;
    struct Node *current = *head, *previous = NULL;
    while (current != NULL && current->number != key) {
        previous = current;
        current = current->next;
    }
    if (current == NULL) return; // Key not found
    if (previous == NULL) *head = current->next; // Deleting the head
    else previous->next = current->next;
    free(current);
}



void deleteByValue(struct Node **head, int value) {
    struct Node *current = *head, *previous = NULL;
    while (current != NULL) {
        if (current->number == value) {
            if (previous == NULL) *head = current->next;
            else previous->next = current->next;
        } else {
            previous = current;
        }
        current = current->next;
    }
}


void insertAfterValue(struct Node **head, int searchValue, int newValue) {
    struct Node *current = *head;
    while (current != NULL && current->number != searchValue) {
        current = current->next;
    }
    if (current == NULL) return; // Value not found
    struct Node *newNode = createNode(newValue);
    newNode->next = current->next;
    current->next = newNode;
}


void printList(struct Node *head) {
    struct Node *current = head;
    printf("[");
    while (current != NULL) {
        printf("%d", current->number);
        current = current->next;

        if(current != NULL){
            printf(", ");
        }
    }
    printf("]\n");
}


int main(){
    struct Node *head = NULL;
    int choice, data;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch(choice){
            case 1:        {
                printList(head);
                break;
            case 2:
                printf("Enter data to append:");
                scanf("%d",&data);
                append(&head,data);
                break;
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case 4:
                printf("Enter key to delete: ");
                scanf("%d", &data);
                deleteByKey(&head, data);
                break;
            case 5:
                printf("Enter value to delete: ");
                scanf("%d", &data);
                deleteByValue(&head, data);
                break;
            case 6:
                printf("Enter key after which to insert: ");
                int key;
                scanf("%d", &key);
                printf("Enter value to insert: ");
                scanf("%d", &data);
                insertAfterKey(&head, key, data);
                break;
            case 7:
                printf("Enter value after which to insert: ");
                scanf("%d", &data);
                printf("Enter new value: ");
                int newValue;
                scanf("%d", &newValue);
                insertAfterValue(&head, data, newValue);
                break;
            case 8:
                exit(0);
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return  0;
}

}

// KABANO TRINITY AMOS 2300726712S

// Questions:
// Implement the prototypes defined above.
// Reimplement the main method using a switch and complete the pending steps.
